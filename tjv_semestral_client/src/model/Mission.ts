import {Platoon} from "./Platoon";

export interface Mission {
    id: number;
    codename?: string;
    platoons?: Platoon[];
}