import { Platoon } from "./Platoon";

export interface Army {
    id: number;
    name: string;
    platoons: Platoon[];
}