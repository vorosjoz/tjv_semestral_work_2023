import {Mission} from "./Mission";

export interface Platoon {
    id: number;
    name: string;
    nickname: string | null;
    idArmy: number;
    missions?: Mission[];
}