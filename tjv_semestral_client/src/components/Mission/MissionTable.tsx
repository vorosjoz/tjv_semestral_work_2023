import {FC} from "react";
import {Mission} from "../../model/Mission";
import '../stylish.css';
import { Link } from 'react-router-dom';

interface MissionTableProps {
    missionData: Mission[];
    setSelectedMission: (mission: Mission | null) => void;
}

export const MissionTable: FC<MissionTableProps> = ({missionData}) => {
    return (
        <table className="table-style">
            <thead>
            <tr>
                <th>ID</th>
                <th>Codename</th>
            </tr>
            </thead>
            <tbody>
            {missionData.map((mission) => (
                <tr key={mission.id}>
                    <td>
                        <Link to={`/mission/${mission.id}`}>
                            {mission.id}
                        </Link>
                    </td>
                    <td>{mission.codename ? mission.codename : "UNKNOWN" }</td>
                </tr>
            ))}
            </tbody>
        </table>
    )
}