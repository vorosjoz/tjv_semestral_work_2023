import {FC, useState} from "react";
import {Mission} from "../../model/Mission";

interface AddMissionProps {
    updateMissionData : () => void;
}

export const AddMission : FC<AddMissionProps> = ({updateMissionData}) => {
    const [codename, setCodename] = useState("");

    const addMission = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        fetch("http://localhost:8080/missions", {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({codename})
        }).then(() => {
            setCodename("");
            updateMissionData(); // Refresh data after adding a mission
        });
    }

    return (
        <form onSubmit={addMission}>
            <label htmlFor="codename">  </label>
            <input type="text" id="codename" name="codename" value={codename} onChange={e => setCodename(e.target.value)} />
            <button type="submit" className="button">Create</button>
        </form>
    )
}