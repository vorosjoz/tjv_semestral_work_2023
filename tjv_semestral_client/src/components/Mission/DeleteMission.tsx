import React, { useState, useEffect } from 'react';
import { Mission } from '../../model/Mission';

interface DeleteMissionProps {
    updateMissionData: () => void;
}

export const DeleteMission: React.FC<DeleteMissionProps> = ({ updateMissionData }) => {
    const [missions, setMissions] = useState<Mission[]>([]);
    const [selectedMission, setSelectedMission] = useState<Mission | null>(null);

    useEffect(() => {
        fetch('http://localhost:8080/missions')
            .then(response => response.json())
            .then(data => setMissions(data));
    }, []);

    const handleSelect = (event: React.ChangeEvent<HTMLSelectElement>) => {
        const selectedId = Number(event.target.value);
        const mission = missions.find(m => m.id === selectedId);
        setSelectedMission(mission || null);
    };

    const handleDelete = (event: React.FormEvent) => {
        event.preventDefault();
        if (selectedMission) {
            fetch(`http://localhost:8080/missions/${selectedMission.id}`, {
                method: 'DELETE',
            })
                .then(() => {
                    updateMissionData();
                });
        }
    };

    return (
        <form onSubmit={handleDelete}>
            <label htmlFor="mission"> Mission </label>
            <select id="mission" name="mission" onChange={handleSelect} required>
                {missions.map((mission) => (
                    <option value={mission.id} key={mission.id}>{mission.codename}</option>
                ))}
            </select>
            <button type="submit" className="button">Delete</button>
        </form>
    );
};