import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { Mission } from '../../model/Mission';
import { Platoon } from '../../model/Platoon';
import '../stylish.css';
import {PlatoonTable} from "../Platoon/PlatoonTable";
import {Army} from "../../model/Army";

export const MissionDetails: React.FC = () => {
    const [armyData, setArmyData] = useState<Army[]>([]);
    const { id } = useParams<{ id: string }>();
    const [mission, setMission] = useState<Mission | null>(null);
    const [platoons, setPlatoons] = useState<Platoon[]>([]);
    const [platoonId, setPlatoonId] = useState<string>('');

    useEffect(() => {
        fetch(`http://localhost:8080/missions/${id}`)
            .then((response) => response.json())
            .then((json) => setMission(json));

        fetch('http://localhost:8080/platoons')
            .then(response => response.json())
            .then(data => setPlatoons(data));
    }, [id]);


    const addPlatoonToMission = () => {
        fetch(`http://localhost:8080/missions/${id}/platoons/${platoonId}`, {
            method: 'POST'
        }).then((response) => {
            if (response.ok) {
                alert('Platoon added to mission successfully');
            } else {
                alert('Failed to add platoon to mission');
            }
        });
    };

    if (!mission) {
        return <div>Loading...</div>;
    }

    return (
        <div className="mission-container">
            <h1>Mission Details</h1>
            <h2> Name : {mission.codename} </h2>
            <h2> ID : {mission.id} </h2>
            <h2>Platoons in this mission : {mission.platoons?.length}</h2>
            {mission.platoons && <PlatoonTable platoonData={mission.platoons} armyData={armyData} />}
            <input
                type="text"
                value={platoonId}
                onChange={(e) => setPlatoonId(e.target.value)}
                placeholder="Enter platoon ID"
            />
            <button className={'homeButton'}
                    onClick={addPlatoonToMission}>Assign This Platoon
            </button>
            <h2>All Platoons :</h2>
            <PlatoonTable platoonData={platoons} armyData={armyData} />
        </div>
    );
};