import React, { useState, useEffect } from 'react';
import { Mission } from '../../model/Mission';
import '../stylish.css'

interface UpdateMissionProps {
    updateMissionData: () => void;
}

export const UpdateMission: React.FC<UpdateMissionProps> = ({ updateMissionData }) => {
    const [missions, setMissions] = useState<Mission[]>([]);
    const [selectedMission, setSelectedMission] = useState<Mission | null>(null);

    useEffect(() => {
        fetch('http://localhost:8080/missions')
            .then(response => response.json())
            .then(data => setMissions(data));
    }, []);

    const handleSubmit = (event: React.FormEvent) => {
        event.preventDefault();
        if (selectedMission) {
            fetch(`http://localhost:8080/missions/${selectedMission.id}`, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(selectedMission),
            })
                .then(() => {
                    updateMissionData();
                });
        }
    };

    const handleSelect = (event: React.ChangeEvent<HTMLSelectElement>) => {
        const selectedId = Number(event.target.value);
        const mission = missions.find(m => m.id === selectedId);
        setSelectedMission(mission || null);
    };

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        if (selectedMission) {
            setSelectedMission({ ...selectedMission, [event.target.name]: event.target.value });
        }
    };

    return (
        <form onSubmit={handleSubmit}>
            <label htmlFor="mission"> Mission </label>
            <select id="mission" name="mission" onChange={handleSelect} required>
                {missions.map((mission) => (
                    <option value={mission.id} key={mission.id}>{mission.id}</option>
                ))}
            </select>
            <label htmlFor="codename"> Codename </label>
            <input type="text" id="codename" name="codename" value={selectedMission?.codename || ''} onChange={handleChange} />
            <button type="submit" className="button">Update</button>
        </form>
    );
};