// MissionPage.tsx
import React, { useEffect, useState } from 'react';
import { Mission } from '../../model/Mission';
import { AddMission } from './AddMission';
import { UpdateMission } from './UpdateMission';
import { DeleteMission } from './DeleteMission';
import {MissionContainer} from "./MissionContainer";

export const MissionPage: React.FC = () => {
    const [missions, setMissions] = useState<Mission[]>([]);

    const updateMissionData = () => {
        fetch('http://localhost:8080/missions')
            .then(response => response.json())
            .then(data => setMissions(data));
    };

    useEffect(() => {
        updateMissionData();
    }, []);

    return (
        <div>
            <MissionContainer/>
        </div>
    );
};