import {useEffect, useState} from "react";
import {Mission} from "../../model/Mission";
import {MissionTable} from "./MissionTable";
import {AddMission} from "./AddMission";
import {DeleteMission} from "./DeleteMission";
import {UpdateMission} from "./UpdateMission";
import '../stylish.css'; // Import the CSS file

export const MissionContainer = () => {
    const [missionData, setMissionData] = useState<Mission[]>([]);
    const [selectedMission, setSelectedMission] = useState<Mission | null>(null);

    const updateMissionData = () => {
        fetch("http://localhost:8080/missions")
            .then((response) => response.json())
            .then((json) => setMissionData(json));
    }

    useEffect(() => {
        updateMissionData();
    }, []);

    return (
        <div className="mission-container"> {/* Add the class here */}
            <h1>Missions</h1>
            <div className="button-stack">
                <AddMission updateMissionData={updateMissionData}/>
                <UpdateMission updateMissionData={updateMissionData}/>
                <DeleteMission updateMissionData={updateMissionData}/>
            </div>
            <MissionTable missionData={missionData} setSelectedMission={setSelectedMission} />
        </div>
    )
}