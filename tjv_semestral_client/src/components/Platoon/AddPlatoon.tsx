import {FC, useEffect, useState} from "react";
import {Army} from "../../model/Army";
import '../stylish.css';
import {PlatoonTable} from "./PlatoonTable";

interface AddPlatoonProps {
    updatePlatoonData : () => void;
}

export const AddPlatoon : FC <AddPlatoonProps> = ( {updatePlatoonData} ) => {
    const [armies, setArmies] = useState<Army[]>([]);

    useEffect(() => {
        fetch("http://localhost:8080/armies")
            .then((response) => response.json())
            .then((json) => setArmies(json));
    }, []);

    const addPlatoon = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        const form = event.target as HTMLFormElement;
        const data = new FormData(form);
        const id = data.get("army") as string;
        const name = data.get("name") as string;
        const nickname = data.get("nickname") as string;
        const platoon: { name: string; nickname: string } = {name, nickname};
        fetch(`http://localhost:8080/armies/${id}/platoons`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(platoon)
        }).then(() => {
            form.reset();
            updatePlatoonData();
        });
    }

    return (
        <form onSubmit={ addPlatoon }>
            <label htmlFor="army">  Army  </label>
            <select id="army" name="army" required>
                {armies.map((army) => (
                    <option value={army.id} key={army.id}>{army.name}</option>
                ))}
            </select>
            <label htmlFor="name">  Platoon Name  </label>
            <input type="text" id="name" name="name" required/>
            <label htmlFor="nickname">  Platoon Nickname  </label>
            <input type="text" id="nickname" name="nickname"/>
            <button type="submit" className="button">Create</button>
        </form>
    )
}
