import {useEffect, useState} from "react";
import {Platoon} from "../../model/Platoon";
import {PlatoonTable} from "./PlatoonTable";
import {AddPlatoon} from "./AddPlatoon";
import {DeletePlatoon} from "./DeletePlatoon";
import {UpdatePlatoon} from "./UpdatePlatoon";
import '../stylish.css';
import {Army} from "../../model/Army"; // Import the CSS file

export const PlatoonContainer = () => {
    const [platoonData, setPlatoonData] = useState<Platoon[]>([]);
    const [armyData, setArmyData] = useState<Army[]>([]);

    const updatePlatoonData = () => {
        fetch("http://localhost:8080/platoons")
            .then((response) => response.json())
            .then((json) => setPlatoonData(json));
    }

    const updateArmyData = () => {
        fetch("http://localhost:8080/armies")
            .then((response) => response.json())
            .then((json) => setArmyData(json));
    }

    useEffect(() => {
        updatePlatoonData();
        updateArmyData();
    }, []);

    return (
        <div className="platoon-container"> {/* Add the class here */}
            <h1>Platoons</h1>
            <div className="button-stack">
                <AddPlatoon updatePlatoonData={updatePlatoonData}/>
                <UpdatePlatoon updatePlatoonData={updatePlatoonData}/>
                <DeletePlatoon updatePlatoonData={updatePlatoonData}/>
            </div>
            <PlatoonTable platoonData={platoonData} armyData={armyData}/>
        </div>
    )
}