import {FC, useEffect, useState} from "react";
import {Platoon} from "../../model/Platoon";
import '../stylish.css';

interface DeletePlatoonProps {
    updatePlatoonData : () => void;
}

export const DeletePlatoon : FC <DeletePlatoonProps> = ( {updatePlatoonData} ) => {
    const [platoons, setPlatoons] = useState<Platoon[]>([]);

    useEffect(() => {
        fetch("http://localhost:8080/platoons")
            .then((response) => response.json())
            .then((json) => setPlatoons(json));
    }, []);

    const deletePlatoon = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        const form = event.target as HTMLFormElement;
        const data = new FormData(form);
        const id = data.get("platoon") as string;
        fetch(`http://localhost:8080/platoons/${id}`, {
            method: "DELETE"
        }).then((response) => {
            if (response.status === 204) {
                form.reset();
                updatePlatoonData(); // Refresh data after deleting a platoon
            } else {
                // handle error status
                console.error(`Error deleting platoon: ${response.status}`);
            }
        });
    }

    return (
        <form onSubmit={ deletePlatoon }>
            <label htmlFor="platoon"> Platoon </label>
            <select id="platoon" name="platoon" required>
                {platoons.map((platoon) => (
                    <option value={platoon.id} key={platoon.id}>{platoon.name}</option>
                ))}
            </select>
            <button type="submit" className="button">Delete</button>
        </form>
    )
}