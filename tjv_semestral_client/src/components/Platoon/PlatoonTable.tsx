import React, {FC} from "react";
import {Platoon} from "../../model/Platoon";
import {Army} from "../../model/Army";
import '../stylish.css';
import { Link } from 'react-router-dom';

interface PlatoonTableProps {
    platoonData: Platoon[];
    armyData: Army[];
}

export const PlatoonTable : FC <PlatoonTableProps> = ( {platoonData, armyData} ) => {
    return (
        <div>
            <table className="table-style">
                <tr>
                    <th>id</th>
                    <th>name</th>
                    <th>nickname</th>
                    <th>army</th>
                </tr>
                {platoonData.map((platoon) => {
                    const armyName = armyData.find(army => army.id === platoon.idArmy)?.name || 'Unknown';
                    return (
                        <tr key={platoon.id}>
                            <td>{platoon.id}</td>
                            <td>
                                <Link to={`/platoon/${platoon.id}`}>
                                    {platoon.name}
                                </Link>
                            </td>
                            <td>{platoon.nickname}</td>
                            <td>{armyName}</td>
                        </tr>
                    )
                })}
            </table>
        </div>
    );
}