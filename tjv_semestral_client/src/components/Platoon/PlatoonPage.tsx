// PlatoonPage.tsx
import React, { useEffect, useState } from 'react';
import { Platoon } from '../../model/Platoon';
import { AddPlatoon } from './AddPlatoon';
import { UpdatePlatoon } from './UpdatePlatoon';
import { DeletePlatoon } from './DeletePlatoon';
import {PlatoonContainer} from "./PlatoonContainer";

export const PlatoonPage: React.FC = () => {
    const [platoons, setPlatoons] = useState<Platoon[]>([]);

    const updatePlatoonData = () => {
        fetch('http://localhost:8080/platoons')
            .then(response => response.json())
            .then(data => setPlatoons(data));
    };

    useEffect(() => {
        updatePlatoonData();
    }, []);

    return (
        <div>
           <PlatoonContainer/>
        </div>
    );
};