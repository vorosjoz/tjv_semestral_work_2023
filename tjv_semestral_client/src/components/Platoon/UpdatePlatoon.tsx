import {FC, useEffect, useState} from "react";
import {Platoon} from "../../model/Platoon";
import {Army} from "../../model/Army";
import '../stylish.css';

interface UpdatePlatoonProps {
    updatePlatoonData : () => void;
}

export const UpdatePlatoon : FC <UpdatePlatoonProps> = ( {updatePlatoonData} ) => {
    const [platoons, setPlatoons] = useState<Platoon[]>([]);
    const [armies, setArmies] = useState<Army[]>([]);

    useEffect(() => {
        fetch("http://localhost:8080/platoons")
            .then((response) => response.json())
            .then((json) => setPlatoons(json));
        fetch("http://localhost:8080/armies")
            .then((response) => response.json())
            .then((json) => setArmies(json));
    }, []);

    const updatePlatoon = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        const form = event.target as HTMLFormElement;
        const data = new FormData(form);
        const id = data.get("platoon") as string;
        const name = data.get("name") as string;
        const nickname = data.get("nickname") as string;
        const armyId = data.get("army") as string;
        fetch(`http://localhost:8080/platoons/${id}`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({name, nickname, armyId})
        }).then(() => {
            form.reset();
            updatePlatoonData();
        });
    }

    return (
        <form onSubmit={ updatePlatoon }>
            <label htmlFor="platoon"> Platoon </label>
            <select id="platoon" name="platoon" required>
                {platoons.map((platoon) => (
                    <option value={platoon.id} key={platoon.id}>{platoon.name}</option>
                ))}
            </select>
            <label htmlFor="name"> New Name </label>
            <input type="text" id="name" name="name" required />
            <label htmlFor="nickname"> New Nickname </label>
            <input type="text" id="nickname" name="nickname" />
            <button type="submit" className="button">Update</button>
        </form>
    )
}