import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { Platoon } from '../../model/Platoon';
import { Army } from '../../model/Army';
import { Mission } from '../../model/Mission';
import '../stylish.css';
import {MissionTable} from "../Mission/MissionTable";

export const PlatoonDetails: React.FC = () => {
    const { id } = useParams<{ id: string }>();
    const [platoon, setPlatoon] = useState<Platoon | null>(null);
    const [army, setArmy] = useState<Army | null>(null);
    const [allMissions, setAllMissions] = useState<Mission[]>([]);
    const [selectedMission, setSelectedMission] = useState<Mission | null>(null);
    const [missionId, setMissionId] = useState<string>('');

    useEffect(() => {
        fetch(`http://localhost:8080/platoons/${id}`)
            .then((response) => response.json())
            .then((json) => setPlatoon(json));

        fetch(`http://localhost:8080/armies/${platoon?.idArmy}`)
            .then((response) => response.json())
            .then((json) => setArmy(json));

        fetch('http://localhost:8080/missions')
            .then(response => response.json())
            .then(data => setAllMissions(data));
    }, [id, platoon?.idArmy]);

    const addMissionToPlatoon = () => {
        fetch(`http://localhost:8080/platoons/${id}/missions/${missionId}`, {
            method: 'POST'
        }).then((response) => {
            if (response.ok) {
                alert('Mission added to platoon successfully');
            } else {
                alert('Failed to add mission to platoon');
            }
        });
    };

    if (!platoon) {
        return <div>Loading...</div>;
    }

    return (
        <div className="platoon-container">
            <h1>Platoon Details</h1>
            <h2> Name : {platoon.name} </h2>
            <h2> ID : {platoon.id} </h2>
            <h2> Army : {army?.name}</h2>
            <h2> Missions : {platoon.missions?.length}</h2>
            <input
                type="text"
                value={missionId}
                onChange={(e) => setMissionId(e.target.value)}
                placeholder="Enter mission ID"
            />
            <button className="homeButton" onClick={addMissionToPlatoon}>Add Mission to Platoon</button>
            <h2>All Missions :</h2>
            <MissionTable missionData={allMissions} setSelectedMission={setSelectedMission} />
            <h2>Platoon {platoon.name} is on these Missions :</h2>
            {platoon.missions && <MissionTable missionData={platoon.missions} setSelectedMission={setSelectedMission} />}
        </div>
    );
};