import {FC, useEffect, useState} from "react";
import {Army} from "../../model/Army";
import '../stylish.css';

interface DeleteArmyProps {
    updateArmyData : () => void;
}

export const DeleteArmy : FC <DeleteArmyProps> = ( {updateArmyData} ) => {
    const [armies, setArmies] = useState<Army[]>([]);

    useEffect(() => {
        fetch("http://localhost:8080/armies")
            .then((response) => response.json())
            .then((json) => setArmies(json));
    }, []);

    const deleteArmy = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        const form = event.target as HTMLFormElement;
        const data = new FormData(form);
        const id = data.get("army") as string;
        fetch(`http://localhost:8080/armies/${id}`, {
            method: "DELETE"
        }).then((response) => {
            if (response.status === 204) {
                form.reset();
                updateArmyData(); // Refresh data after deleting an army
            } else {
                // handle error status
                console.error(`Error deleting army: ${response.status}`);
            }
        });
    }

    return (
        <form onSubmit={ deleteArmy }>
            <label htmlFor="army"> Army </label>
            <select id="army" name="army" required>
                {armies.map((army) => (
                    <option value={army.id} key={army.id}>{army.name}</option>
                ))}
            </select>
            <button type="submit" className="button">Delete</button>
        </form>
    )
}