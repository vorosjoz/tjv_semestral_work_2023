// ArmyPage.tsx
import React, { useEffect, useState } from 'react';
import { Army } from '../../model/Army';
import { AddArmy } from './AddArmy';
import { UpdateArmy } from './UpdateArmy';
import { DeleteArmy } from './DeleteArmy';
import '../index.ts'
import {ArmyContainer} from "./ArmyContainer";

export const ArmyPage: React.FC = () => {
    const [armies, setArmies] = useState<Army[]>([]);

    const updateArmyData = () => {
        fetch('http://localhost:8080/armies')
            .then(response => response.json())
            .then(data => setArmies(data));
    };

    useEffect(() => {
        updateArmyData();
    }, []);

    return (
        <div>
            <ArmyContainer/>
        </div>
    );
};