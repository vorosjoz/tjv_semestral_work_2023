import {FC, useEffect, useState} from "react";
import {Army} from "../../model/Army";
import React from "react";

interface UpdateArmyProps {
    updateArmyData : () => void;
}

export const UpdateArmy : FC <UpdateArmyProps> = ( {updateArmyData} ) => {
    const [armies, setArmies] = useState<Army[]>([]);

    useEffect(() => {
        fetch("http://localhost:8080/armies")
            .then((response) => response.json())
            .then((json) => setArmies(json));
    }, []);

    const updateArmy = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        const form = event.target as HTMLFormElement;
        const data = new FormData(form);
        const id = data.get("army") as string;
        const name = data.get("name") as string;
        fetch(`http://localhost:8080/armies/${id}`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({name})
        }).then(() => {
            form.reset();
            updateArmyData();
        });
    }

    return (
        <form onSubmit={updateArmy}>
            <label htmlFor="army"> Army</label>
            <select id="army" name="army" required>
                {armies.map((army) => (
                    <option value={army.id} key={army.id}>{army.name}</option>
                ))}
            </select>
            <label htmlFor="name"> New Name </label>
            <input type="text" id="name" name="name" required/>
            <button type="submit" className="button">Update</button>
        </form>
    )
}