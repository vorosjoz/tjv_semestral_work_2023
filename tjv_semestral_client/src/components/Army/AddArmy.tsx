import {FC, useEffect, useState} from "react";
import '../stylish.css';
import {Army} from "../../model/Army";

interface AddArmyProps {
    updateArmyData : () => void;
}

export const AddArmy : FC <AddArmyProps> = ( {updateArmyData} ) => {
    const [armies, setArmies] = useState<Army[]>([]);

    useEffect(() => {
        fetch("http://localhost:8080/armies")
            .then((response) => response.json())
            .then((json) => setArmies(json));
    }, []);

    const addArmy = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        const form = event.target as HTMLFormElement;
        const data = new FormData(form);
        const name = data.get("name") as string;
        fetch("http://localhost:8080/armies", {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({name})
        }).then(() => {
            form.reset();
            updateArmyData(); // Refresh data after adding an army
        });
    }

    return (
        <form onSubmit={addArmy}>
            <label htmlFor="name"> Army Name </label>
            <input type="text" id="name" name="name" required/>
            <button type="submit" className="button">Create</button>
        </form>
    )
}