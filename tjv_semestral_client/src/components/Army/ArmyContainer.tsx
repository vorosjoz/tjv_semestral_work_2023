import {AddArmy} from "./AddArmy";
import {ArmyTable} from "./ArmyTable";
import {useEffect, useState} from "react";
import {Army} from "../../model/Army";
import {DeleteArmy} from "./DeleteArmy";
import {UpdateArmy} from "./UpdateArmy";

export const ArmyContainer = () => {
    const[armyData,setArmyData] = useState<Army[]>([])
    const updateArmyData = () => {
        fetch("http://localhost:8080/armies")
            .then((response) => response.json())
            .then((json) => setArmyData(json));
    }

    useEffect(() => {
        updateArmyData();
    }, []);

    return (
        <div className="army-container" >
            <h1>Armies</h1>
            <div className="button-stack">
                <AddArmy updateArmyData={updateArmyData}/>
                <UpdateArmy updateArmyData={updateArmyData}/>
                <DeleteArmy updateArmyData={updateArmyData}/>
            </div>
            <ArmyTable armyData = {armyData}/>
        </div>
    )
}