import React, {FC} from "react";
import {Army} from "../../model/Army";
import '../stylish.css';
import { Link } from 'react-router-dom';

interface ArmyTableProps {
    armyData: Army[];
}

export const ArmyTable : FC <ArmyTableProps> = ( {armyData} ) => {

    return (
        <div>
            <table className="table-style">
                <tr>
                    <th>
                        id

                    </th>
                    <th>
                        name
                    </th>
                    <th>
                        platoons
                    </th>
                </tr>
                {armyData.map((army) => (
                    <tr key={army.id}>
                        <td>
                            {army.id}
                        </td>
                        <td>
                            <Link to={`/army/${army.id}`}>
                            {army.name}
                            </Link>
                        </td>
                        <td>
                            {army.platoons.length}
                        </td>
                    </tr>
                ))}
            </table>
        </div>
    );
}



