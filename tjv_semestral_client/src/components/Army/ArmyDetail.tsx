import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { Army } from '../../model/Army';
import { PlatoonTable } from '../Platoon/PlatoonTable';
import {Platoon} from "../../model/Platoon";
import '../stylish.css';

export const ArmyDetail: React.FC = () => {
    const { id } = useParams<{ id: string }>();
    const [army, setArmy] = useState<Army | null>(null);
    const [platoonData, setPlatoonData] = useState<Platoon[]>([]);

    useEffect(() => {
        fetch(`http://localhost:8080/armies/${id}`)
            .then((response) => response.json())
            .then((json) => setArmy(json));

        fetch(`http://localhost:8080/platoons`)
            .then((response) => response.json())
            .then((json) => setPlatoonData(json));
    }, [id]);

    if (!army) {
        return <div>Loading...</div>;
    }

    const armyPlatoons = platoonData.filter(platoon => platoon.idArmy === army.id);

    return (
        <div className="platoon-container">
            <h1>Army Details</h1>
            <h2> Name : {army.name} </h2>
            <h2> ID : {army.id} </h2>
            <h2> Platoons: {army.platoons.length}</h2>
            <PlatoonTable armyData={[army]} platoonData={armyPlatoons} />
        </div>
    );
};