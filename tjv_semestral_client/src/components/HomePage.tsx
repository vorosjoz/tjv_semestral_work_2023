import React from 'react';
import { Link } from 'react-router-dom';
import './stylish.css';

export const HomePage: React.FC = () => {
    return (
        <div className="button-container">
            <Link to="/army"><button className="homeButton">Army</button></Link>
            <Link to="/platoon"><button className="homeButton">Platoon</button></Link>
            <Link to="/mission"><button className="homeButton">Mission</button></Link>
        </div>
    );
};