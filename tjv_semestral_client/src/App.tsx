import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import { HomePage } from './components/HomePage';
import { ArmyPage } from './components/Army/ArmyPage';
import { PlatoonPage } from './components/Platoon/PlatoonPage';
import { MissionPage } from './components/Mission/MissionPage';
import { ArmyDetail } from './components/Army/ArmyDetail';
import {PlatoonDetails} from "./components/Platoon/PlatoonsDetail";
import {MissionDetails} from "./components/Mission/MissionDetail";

export const App: React.FC = () => {
    return (
        <Router>
            <Routes>
                <Route path="/army/:id" element={<ArmyDetail />} />
                <Route path="/platoon/:id" element = {<PlatoonDetails/>}/>
                <Route path="/mission/:id" element = {<MissionDetails/>}/>
                <Route path="/army" element={<ArmyPage />} />
                <Route path="/platoon" element={<PlatoonPage />} />
                <Route path="/mission" element={<MissionPage />} />
                <Route path="/platoons-on-missions" element={<></>} />
                <Route path="/" element={<HomePage />} />
            </Routes>
        </Router>
    );
};